package com.example.demo.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.CVoucher;

// xài interface chứ ko xài static hay abstract dc vì như vậy sẽ khai báo lại từ đầu , và ko tận dụng dc thư viện
public interface IvoucherRespository extends JpaRepository<CVoucher ,Long>{
    
}
