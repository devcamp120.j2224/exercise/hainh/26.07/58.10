package com.example.demo.Controller;
import com.example.demo.Model.CVoucher;
import com.example.demo.Respository.IvoucherRespository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class CVoucherController {

    //bắt buộc fai xài autoWired vì nó là thư viện hibernate 
    @Autowired
    IvoucherRespository repository;

    @GetMapping("/vouchers")
    public ResponseEntity <List <CVoucher>> getVouchers(){

        try {
            List<CVoucher> listVoucher = new ArrayList<CVoucher>();
            // object. findAll là hàm tìm kiếm tất cả . forEach và add hết tất cả cho arraylist 
            repository.findAll().forEach(listVoucher :: add);

            if(listVoucher.size() == 0){
                return new ResponseEntity<List <CVoucher>>(listVoucher, HttpStatus.NOT_FOUND);
            }
            else{
                return new ResponseEntity<List <CVoucher>>(listVoucher, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
